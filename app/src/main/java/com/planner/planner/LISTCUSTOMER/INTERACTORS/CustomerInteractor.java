package com.planner.planner.LISTCUSTOMER.INTERACTORS;

import com.planner.planner.LISTCUSTOMER.MODEL.CustomerModel;

public interface CustomerInteractor {

    void getListCutomer(String userId);
    void deleteCustomer(CustomerModel customerModel);
    void editCustomer(CustomerModel customerModel);
}
