package com.planner.planner.LISTCUSTOMER.VIEW;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import com.planner.planner.LISTCUSTOMER.MODEL.CustomerModel;
import com.planner.planner.R;
import com.planner.planner.UTIL.ValidationClass;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

@SuppressLint("ValidFragment")
public class EditCustomerDialog extends DialogFragment {

    public interface OnGetData{
        void onGetEdit(CustomerModel customerModel);
        void onGetDelete(CustomerModel customerModel);
    }

    private OnGetData listener=null;
    private CustomerModel customerModel;

    @SuppressLint("ValidFragment")
    public EditCustomerDialog(CustomerModel customerModel, OnGetData listener){
        this.listener = listener;
        this.customerModel = customerModel;
    }

    @BindView(R.id.etUderId)
    EditText etUserId;
    @BindView(R.id.etName)
    EditText etName;
    @BindView(R.id.etLastName)
    EditText etLastName;
    @BindView(R.id.etAddress)
    EditText etAddress;
    @BindView(R.id.etPhone)
    EditText etPhone;

    @BindView(R.id.rbPending)
    RadioButton rbPending;
    @BindView(R.id.rbApproved)
    RadioButton rbApproved;
    @BindView(R.id.rbAccepted)
    RadioButton rbAccepted;
    @BindView(R.id.rbRejected)
    RadioButton rbRejected;
    @BindView(R.id.rbDisabeld)
    RadioButton rbDisabeld;

    @BindView(R.id.lyGeneral)
    LinearLayout lyGeneral;

    private View view;

    private String stateCustomer = "0";

    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.dialog_detail_customer, null);
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        etUserId.setText(this.customerModel.getCustomerId());
        etName.setText(this.customerModel.getNombre());
        etLastName.setText(this.customerModel.getApellido());
        etAddress.setText(this.customerModel.getDireccion());
        etPhone.setText(this.customerModel.getTelefono());

        this.view = view;
        switch (this.customerModel.getEstado()){
            case "0":
                stateCustomer = "0";
                this.view.setBackgroundColor(Color.YELLOW);
                rbPending.setChecked(true);
                break;
            case "1":
                stateCustomer = "1";
                this.view.setBackgroundColor(Color.BLUE);
                rbApproved.setChecked(true);
                break;
            case "2":
                stateCustomer = "2";
                this.view.setBackgroundColor(Color.GREEN);
                rbAccepted.setChecked(true);
                break;
            case "3":
                stateCustomer = "3";
                this.view.setBackgroundColor(Color.RED);
                rbRejected.setChecked(true);
                break;
            case "4":
                stateCustomer = "4";
                this.view.setBackgroundColor(Color.GRAY);
                rbDisabeld.setChecked(true);
                break;
        }


        builder.setView(view);

        return builder.create();
    }

    @OnClick(R.id.btEdit)
    public void editCustomer(){

        if(new ValidationClass().ValidacionCamposVacios(getContext(), lyGeneral)){
            this.customerModel.setNombre(etName.getText().toString());
            this.customerModel.setApellido(etLastName.getText().toString());
            this.customerModel.setDireccion(etAddress.getText().toString());
            this.customerModel.setTelefono(etPhone.getText().toString());
            this.customerModel.setEstado(stateCustomer);

            this.listener.onGetEdit(this.customerModel);
            this.getDialog().cancel();
        }
    }

    @OnClick(R.id.btDelete)
    public void deleteCustomer(){
        this.listener.onGetDelete(this.customerModel);
        this.getDialog().cancel();
    }


    @OnClick(R.id.rbPending)
    public void changeStatePeding(View view){
        stateCustomer = "0";
        this.view.setBackgroundColor(Color.YELLOW);
    }

    @OnClick(R.id.rbApproved)
    public void changeStateapproved(View view){
        stateCustomer = "1";
        this.view.setBackgroundColor(Color.BLUE);
    }

    @OnClick(R.id.rbAccepted)
    public void changeStateAccepted(View view){
        stateCustomer = "2";
        this.view.setBackgroundColor(Color.GREEN);
    }

    @OnClick(R.id.rbRejected)
    public void changeStateRejected(View view){

        stateCustomer = "3";
        this.view.setBackgroundColor(Color.RED);
    }

    @OnClick(R.id.rbDisabeld)
    public void changeStateDisabled(View view){
        stateCustomer = "4";
        this.view.setBackgroundColor(Color.GRAY);
    }


}
