package com.planner.planner.LISTCUSTOMER.REPOSITORY;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.planner.planner.LISTCUSTOMER.MODEL.CustomerModel;
import com.planner.planner.LISTCUSTOMER.PRESENTERS.CustomerPresenterImpl;
import com.planner.planner.R;
import com.planner.planner.UTIL.SetAndGetDataStorageIntern;

import java.util.List;

import static com.planner.planner.UTIL.GlobalConst.KEYSaveDataListUsers;


public class CustomerDeleteRepositoryImpl implements CustomerDeleteRepository {
    CustomerPresenterImpl customerPresenter;
    Context context;
    SharedPreferences sharedPreferences;
    SetAndGetDataStorageIntern setAndGetDataStorageIntern;
    String TAG = "CustomerDeleteRepositoryImpl";
    public CustomerDeleteRepositoryImpl(Context context, CustomerPresenterImpl customerPresenter) {
        this.customerPresenter = customerPresenter;
        this.context = context;

        setAndGetDataStorageIntern = new SetAndGetDataStorageIntern(this.context);
    }
    @Override
    public void deleteCustomer(CustomerModel customerModel) {

        String jsonData = setAndGetDataStorageIntern.getDataUser(KEYSaveDataListUsers);
        if(!jsonData.equals("")){
            Log.v(TAG, "deleteCustomer : " + jsonData);
            List<CustomerModel> customerModels = new Gson().fromJson(jsonData, new TypeToken<List<CustomerModel>>(){}.getType());

            for(CustomerModel customerModel1 : customerModels){
                if(customerModel1.getCustomerId().equals(customerModel.getCustomerId())){
                    customerModels.remove(customerModel1);
                }
            }
            setAndGetDataStorageIntern.saveDataUser(KEYSaveDataListUsers, new Gson().toJson(customerModels));


            jsonData = setAndGetDataStorageIntern.getDataUser(KEYSaveDataListUsers);

            customerModels = new Gson().fromJson(jsonData, new TypeToken<List<CustomerModel>>(){}.getType());

            Log.v(TAG, "deleteCustomer output : " + jsonData);
            this.customerPresenter.showListCustomer(customerModels);

        }else {
            this.customerPresenter.showError(context.getString(R.string.messagge_cutomer_no_existe));
        }
    }
}
