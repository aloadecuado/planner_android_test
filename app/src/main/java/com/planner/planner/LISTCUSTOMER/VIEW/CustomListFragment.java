package com.planner.planner.LISTCUSTOMER.VIEW;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.planner.planner.LISTCUSTOMER.PRESENTERS.CustomerPresenterImpl;
import com.planner.planner.LOGIN.MODEL.UserModel;
import com.planner.planner.R;
import com.planner.planner.LISTCUSTOMER.MODEL.CustomerModel;

import java.util.List;

import static com.planner.planner.UTIL.GlobalVar.USEROBJECT;

public class CustomListFragment extends Fragment implements CustomListFragmentView{



    RecyclerView recyclerView;

    CustomerPresenterImpl customerPresenter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_customlist_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            recyclerView = (RecyclerView) view;

            recyclerView.setLayoutManager(new LinearLayoutManager(context));

        }
        customerPresenter = new CustomerPresenterImpl(getContext(),this);
        customerPresenter.getListCutomer(USEROBJECT.getUserId());
        return view;
    }

    private void loadCustomer(List<CustomerModel> customerModels){
        recyclerView.setAdapter(new MyCustomListRecyclerViewAdapter(customerModels, new MyCustomListRecyclerViewAdapter.OnGetData() {
            @Override
            public void onGedClick(View view, CustomerModel customerModel) {

                FragmentManager fm = getActivity().getSupportFragmentManager();
                EditCustomerDialog addPlanDialog = new EditCustomerDialog(customerModel, new EditCustomerDialog.OnGetData() {
                    @Override
                    public void onGetEdit(CustomerModel customerModel) {
                        customerPresenter.editCustomer(customerModel);
                    }

                    @Override
                    public void onGetDelete(CustomerModel customerModel) {
                        customerPresenter.deleteCustomer(customerModel);
                    }
                });


                addPlanDialog.show(fm, "fragment_edit_customer");
            }
        }));
    }
    @Override
    public void showListCustomer(List<CustomerModel> customerModels) {
        loadCustomer(customerModels);
    }

    @Override
    public void showError(String error) {
        Toast.makeText(getContext(), error, Toast.LENGTH_SHORT).show();

    }



}
