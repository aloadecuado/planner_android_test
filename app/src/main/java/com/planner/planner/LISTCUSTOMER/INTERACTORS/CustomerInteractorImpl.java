package com.planner.planner.LISTCUSTOMER.INTERACTORS;

import android.content.Context;

import com.planner.planner.LISTCUSTOMER.MODEL.CustomerModel;
import com.planner.planner.LISTCUSTOMER.PRESENTERS.CustomerPresenterImpl;
import com.planner.planner.LISTCUSTOMER.REPOSITORY.CustomerDeleteRepositoryImpl;
import com.planner.planner.LISTCUSTOMER.REPOSITORY.CustomerEditRepositoryimpl;
import com.planner.planner.LISTCUSTOMER.REPOSITORY.CustomerRepositoryImpl;

public class CustomerInteractorImpl implements CustomerInteractor {
    CustomerPresenterImpl customerPresenter;
    CustomerRepositoryImpl customerRepository;
    CustomerEditRepositoryimpl customerEditRepository;
    CustomerDeleteRepositoryImpl customerDeleteRepository;
    public CustomerInteractorImpl(Context context, CustomerPresenterImpl customerPresenter) {
        this.customerPresenter = customerPresenter;
        customerRepository= new CustomerRepositoryImpl(context, this.customerPresenter);
        customerEditRepository= new CustomerEditRepositoryimpl(context, this.customerPresenter);
        customerDeleteRepository= new CustomerDeleteRepositoryImpl(context, this.customerPresenter);

    }

    @Override
    public void getListCutomer(String userId) {
        customerRepository.getListCutomer(userId);
    }

    @Override
    public void deleteCustomer(CustomerModel customerModel) {
        customerDeleteRepository.deleteCustomer(customerModel);
    }

    @Override
    public void editCustomer(CustomerModel customerModel) {
        customerEditRepository.editCustomer(customerModel);
    }
}
