package com.planner.planner.LISTCUSTOMER.REPOSITORY;

import com.planner.planner.LISTCUSTOMER.MODEL.CustomerModel;

import java.util.List;

public interface CustomerEditRepository {
    void editCustomer(CustomerModel customerModel);
}
