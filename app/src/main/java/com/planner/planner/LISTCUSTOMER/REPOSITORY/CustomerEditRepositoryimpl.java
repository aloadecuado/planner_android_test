package com.planner.planner.LISTCUSTOMER.REPOSITORY;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.planner.planner.LISTCUSTOMER.MODEL.CustomerModel;
import com.planner.planner.LISTCUSTOMER.PRESENTERS.CustomerPresenterImpl;
import com.planner.planner.R;
import com.planner.planner.UTIL.SetAndGetDataStorageIntern;

import java.util.List;

import static com.planner.planner.UTIL.GlobalConst.KEYSaveDataListUsers;
import static com.planner.planner.UTIL.GlobalConst.KEYSaveDataLogs;


public class CustomerEditRepositoryimpl implements CustomerEditRepository {

    CustomerPresenterImpl customerPresenter;
    Context context;
    SharedPreferences sharedPreferences;

    SetAndGetDataStorageIntern setAndGetDataStorageIntern;
    String TAG = "CustomerRepositoryImpl";
    public CustomerEditRepositoryimpl(Context context, CustomerPresenterImpl customerPresenter) {
        this.customerPresenter = customerPresenter;
        this.context = context;
        setAndGetDataStorageIntern = new SetAndGetDataStorageIntern(this.context);
    }

    @Override
    public void editCustomer(CustomerModel customerModel) {

        String jsonData = setAndGetDataStorageIntern.getDataUser(KEYSaveDataListUsers);
        String Logs = setAndGetDataStorageIntern.getDataUser(KEYSaveDataLogs);
        if(!jsonData.equals("")){
            Log.v(TAG, "editCustomer : " + jsonData);
            List<CustomerModel> customerModels = new Gson().fromJson(jsonData, new TypeToken<List<CustomerModel>>(){}.getType());

            int i = 0;
            for(CustomerModel customerModel1 : customerModels){
                if(customerModel1.getCustomerId().equals(customerModel.getCustomerId())){
                    String name = "";
                    String lastName = "";
                    String phone = "";
                    String address = "";
                    String state = "";

                    if(!customerModel1.getNombre().equals(customerModel.getNombre())){
                        name = " cambio nombre de: " + customerModel1.getNombre()+ " a: " + customerModel.getNombre(); ;
                    }
                    if(!customerModel1.getApellido().equals(customerModel.getApellido())){
                        lastName = " cambio apellid de: " + customerModel1.getApellido()+ " a: " + customerModel.getApellido(); ;
                    }
                    if(!customerModel1.getTelefono().equals(customerModel.getTelefono())){
                        phone = " cambio telefono de: " + customerModel1.getTelefono()+ " a: " + customerModel.getTelefono(); ;
                    }
                    if(!customerModel1.getDireccion().equals(customerModel.getDireccion())){
                        address = " cambio direccion de: " + customerModel1.getDireccion()+ " a: " + customerModel.getDireccion(); ;
                    }
                    if(!customerModel1.getEstado().equals(customerModel.getEstado())){
                        state = " cambio estado de: " + customerModel1.getEstado()+ " a: " + customerModel.getEstado(); ;
                    }

                    Logs = Logs + "el Usuario con id: " + 3 + name + lastName + phone + address + state + "@@";

                    setAndGetDataStorageIntern.saveDataUser(KEYSaveDataLogs, Logs);
                    customerModels.set(i, customerModel);
                }
                i++;
            }

            setAndGetDataStorageIntern.saveDataUser(KEYSaveDataListUsers, new Gson().toJson(customerModels));


            jsonData = setAndGetDataStorageIntern.getDataUser(KEYSaveDataListUsers);

            customerModels = new Gson().fromJson(jsonData, new TypeToken<List<CustomerModel>>(){}.getType());

            Log.v(TAG, "editCustomer output : " + jsonData);
            this.customerPresenter.showListCustomer(customerModels);

        }else {
            this.customerPresenter.showError(context.getString(R.string.messagge_cutomer_no_existe));
        }
    }
}
