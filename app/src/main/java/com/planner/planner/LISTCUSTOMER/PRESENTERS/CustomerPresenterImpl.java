package com.planner.planner.LISTCUSTOMER.PRESENTERS;

import android.content.Context;

import com.planner.planner.LISTCUSTOMER.INTERACTORS.CustomerInteractorImpl;
import com.planner.planner.LISTCUSTOMER.MODEL.CustomerModel;
import com.planner.planner.LISTCUSTOMER.VIEW.CustomListFragmentView;

import java.util.List;

public class CustomerPresenterImpl implements CustomerPresenter {
    CustomerInteractorImpl customerInteractor;
    CustomListFragmentView customListFragmentView;
    public CustomerPresenterImpl(Context context, CustomListFragmentView customListFragmentView) {
        this.customListFragmentView = customListFragmentView;
        customerInteractor = new CustomerInteractorImpl(context, this);
    }

    @Override
    public void getListCutomer(String userId) {
        customerInteractor.getListCutomer(userId);
    }

    @Override
    public void deleteCustomer(CustomerModel customerModel) {
        customerInteractor.deleteCustomer(customerModel);
    }

    @Override
    public void editCustomer(CustomerModel customerModel) {
        customerInteractor.editCustomer(customerModel);
    }

    @Override
    public void showListCustomer(List<CustomerModel> customerModels) {
        this.customListFragmentView.showListCustomer(customerModels);
    }

    @Override
    public void showError(String error) {
        this.customListFragmentView.showError(error);
    }
}
