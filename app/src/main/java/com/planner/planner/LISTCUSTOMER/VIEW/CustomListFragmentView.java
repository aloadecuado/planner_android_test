package com.planner.planner.LISTCUSTOMER.VIEW;

import com.planner.planner.LISTCUSTOMER.MODEL.CustomerModel;

import java.util.List;

public interface CustomListFragmentView {
    void showListCustomer(List<CustomerModel> customerModels);
    void showError(String error);
}
