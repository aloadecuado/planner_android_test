package com.planner.planner.LISTCUSTOMER.REPOSITORY;

import com.planner.planner.LISTCUSTOMER.MODEL.CustomerModel;

public interface CustomerDeleteRepository {
    void deleteCustomer(CustomerModel customerModel);
}
