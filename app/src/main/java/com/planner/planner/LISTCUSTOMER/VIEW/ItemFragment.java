package com.planner.planner.LISTCUSTOMER.VIEW;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.planner.planner.LISTCUSTOMER.MODEL.CustomerModel;
import com.planner.planner.LISTCUSTOMER.PRESENTERS.CustomerPresenterImpl;
import com.planner.planner.R;
import com.planner.planner.UTIL.SetAndGetDataStorageIntern;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.planner.planner.UTIL.GlobalConst.KEYSaveDataListUsers;
import static com.planner.planner.UTIL.GlobalConst.KEYSaveDataLogs;


public class ItemFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;

    List<String> Logs;
    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ItemFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static ItemFragment newInstance(int columnCount) {
        ItemFragment fragment = new ItemFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }
    RecyclerView recyclerView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_item_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            setList();
        }


        return view;
    }

    private void setList(){

        SetAndGetDataStorageIntern setAndGetDataStorageIntern = new SetAndGetDataStorageIntern(getContext());
        String datos = setAndGetDataStorageIntern.getDataUser(KEYSaveDataLogs);
        Logs = Arrays.asList(datos.split("@@"));

        recyclerView.setAdapter(new MyItemRecyclerViewAdapter(Logs));
    }



}
