package com.planner.planner.LISTCUSTOMER.PRESENTERS;

import com.planner.planner.LISTCUSTOMER.MODEL.CustomerModel;

import java.util.List;

public interface CustomerPresenter {

    void getListCutomer(String userId);
    void deleteCustomer(CustomerModel customerModel);
    void editCustomer(CustomerModel customerModel);

    void showListCustomer(List<CustomerModel> customerModels);
    void showError(String error);
}
