package com.planner.planner.LISTCUSTOMER.REPOSITORY;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.planner.planner.LISTCUSTOMER.MODEL.CustomerModel;
import com.planner.planner.LISTCUSTOMER.PRESENTERS.CustomerPresenterImpl;
import com.planner.planner.UTIL.SetAndGetDataStorageIntern;

import java.util.List;

import static android.content.Context.MODE_PRIVATE;
import static com.planner.planner.UTIL.GlobalConst.KEYSaveDataListUsers;
import static com.planner.planner.UTIL.GlobalConst.KURLListCutomer;


public class CustomerRepositoryImpl implements CustomerRepository {

    CustomerPresenterImpl customerPresenter;
    Context context;
    SharedPreferences sharedPreferences;

    SetAndGetDataStorageIntern setAndGetDataStorageIntern;

    String TAG = "CustomerRepositoryImpl";
    public CustomerRepositoryImpl(Context context, CustomerPresenterImpl customerPresenter) {
      this.customerPresenter = customerPresenter;
      this.context = context;
        setAndGetDataStorageIntern = new SetAndGetDataStorageIntern(this.context);
    }

    @Override
    public void getListCutomer(String userId) {

        String arrayJsonCustom = setAndGetDataStorageIntern.getDataUser(KEYSaveDataListUsers);
        if(!arrayJsonCustom.equals("")){

            List<CustomerModel> customerModels = new Gson().fromJson(arrayJsonCustom, new TypeToken<List<CustomerModel>>(){}.getType());
            customerPresenter.showListCustomer(customerModels);
            Log.v(TAG, "Save data list customer : " + arrayJsonCustom);
            return;
        }
        RequestQueue queue = Volley.newRequestQueue(context);

        String url = KURLListCutomer.replace("{UserId}", userId);
        Log.v(TAG, "url get list: " + url);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.v(TAG, "Save online list customer : " + response);
                        List<CustomerModel> customerModels = new Gson().fromJson(response, new TypeToken<List<CustomerModel>>(){}.getType());
                        setAndGetDataStorageIntern.saveDataUser(KEYSaveDataListUsers, response);

                        customerPresenter.showListCustomer(customerModels);
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.v(TAG, "Save online list customer error : " + error.getLocalizedMessage());
                        customerPresenter.showError(error.getLocalizedMessage());
                    }
                });

        queue.add(stringRequest);
    }

}
