package com.planner.planner.LISTCUSTOMER.VIEW;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.planner.planner.R;
import com.planner.planner.LISTCUSTOMER.MODEL.CustomerModel;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link CustomerModel} and makes a call to the
 * TODO: Replace the implementation with code for your data type.
 */
public class MyCustomListRecyclerViewAdapter extends RecyclerView.Adapter<MyCustomListRecyclerViewAdapter.ViewHolder> {

    private final List<CustomerModel> mValues;

    public interface OnGetData{
        void onGedClick(View view, CustomerModel customerModel);
    }

    private OnGetData listener;
    public MyCustomListRecyclerViewAdapter(List<CustomerModel> items, OnGetData listener) {
        mValues = items;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_customlist, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mItem = mValues.get(position);
        holder.lbName.setText("name: " +holder.mItem.getNombre() + " " + holder.mItem.getApellido()  );
        holder.lbAddress.setText("Address: " + holder.mItem.getDireccion());
        holder.lbPhone.setText("Phone: " + holder.mItem.getTelefono());

        switch (this.mValues.get(position).getEstado()){
            case "0":
                holder.itemView.setBackgroundColor(Color.YELLOW);
                break;
            case "1":

                holder.itemView.setBackgroundColor(Color.BLUE);

                break;
            case "2":

                holder.itemView.setBackgroundColor(Color.GREEN);

                break;
            case "3":

                holder.itemView.setBackgroundColor(Color.RED);

                break;
            case "4":

                holder.itemView.setBackgroundColor(Color.GRAY);

                break;
        }
        holder.btDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               listener.onGedClick(v, mValues.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView lbName;
        public final TextView lbAddress;
        public final TextView lbPhone;
        public final Button btDetail;
        public CustomerModel mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            lbName = (TextView) view.findViewById(R.id.lbFullName);
            lbAddress = (TextView) view.findViewById(R.id.lbAdrress);
            lbPhone = (TextView) view.findViewById(R.id.lbPhone);

            btDetail = (Button) view.findViewById(R.id.btDetail);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + lbName.getText() + "'";
        }
    }
}
