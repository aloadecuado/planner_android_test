package com.planner.planner.UTIL;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.planner.planner.R;

import java.util.ArrayList;


/**
 * Created by pedrodaza on 24/01/18.
 */

public class ValidationClass {

    public Boolean ValidacionCamposVacios(Context ctx, LinearLayout linearLayout)
    {
        Boolean Valido = true;
        ArrayList<TextInputLayout> myTextInputLayout = new ArrayList<TextInputLayout>();
        ArrayList<EditText> myEditTextList = new ArrayList<EditText>();

        for( int i = 0; i < linearLayout.getChildCount(); i++ ) {
            if (linearLayout.getChildAt(i) instanceof TextInputLayout) {
                TextInputLayout textInputLayout = (TextInputLayout) linearLayout.getChildAt(i);
                myTextInputLayout.add(textInputLayout);
                for( int j = 0; j < textInputLayout.getChildCount(); j++ )
                {
                    if (textInputLayout.getChildAt(j) instanceof FrameLayout) {
                        FrameLayout frameLayout = (FrameLayout) textInputLayout.getChildAt(j);
                        if (frameLayout.getChildAt(0) instanceof EditText)
                            myEditTextList.add((EditText) ((FrameLayout)textInputLayout.getChildAt(j)).getChildAt(0));
                    }

                }

            }
            if (linearLayout.getChildAt(i) instanceof EditText)
                myEditTextList.add((EditText) linearLayout.getChildAt(i));
        }

        int k = 0;
        for (TextInputLayout editText : myTextInputLayout)
        {
            if(myEditTextList.get(k).getText().toString().trim().isEmpty())
            {
                myTextInputLayout.get(k).setError(ctx.getString(R.string.error_Vacio));
                Valido = false;
            }
            else
            {
                myTextInputLayout.get(k).setError(ctx.getString(R.string.error_Vacio_sinerror));
            }

            k++;
        }
        return Valido;
    }

    public Boolean cleanCamps(Context ctx, LinearLayout linearLayout)
    {
        Boolean Valido = true;
        ArrayList<TextInputLayout> myTextInputLayout = new ArrayList<TextInputLayout>();
        ArrayList<EditText> myEditTextList = new ArrayList<EditText>();

        for( int i = 0; i < linearLayout.getChildCount(); i++ ) {
            if (linearLayout.getChildAt(i) instanceof TextInputLayout) {
                TextInputLayout textInputLayout = (TextInputLayout) linearLayout.getChildAt(i);
                myTextInputLayout.add(textInputLayout);
                for( int j = 0; j < textInputLayout.getChildCount(); j++ )
                {
                    if (textInputLayout.getChildAt(j) instanceof FrameLayout) {
                        FrameLayout frameLayout = (FrameLayout) textInputLayout.getChildAt(j);
                        if (frameLayout.getChildAt(0) instanceof EditText)
                            myEditTextList.add((EditText) ((FrameLayout)textInputLayout.getChildAt(j)).getChildAt(0));
                    }

                }

            }
            if (linearLayout.getChildAt(i) instanceof EditText)
                myEditTextList.add((EditText) linearLayout.getChildAt(i));
        }

        int k = 0;
        for (TextInputLayout editText : myTextInputLayout)
        {
            myEditTextList.get(k).setText("");

            k++;
        }
        return Valido;
    }
}
