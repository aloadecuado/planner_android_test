package com.planner.planner.UTIL;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.planner.planner.LISTCUSTOMER.PRESENTERS.CustomerPresenterImpl;

import static android.content.Context.MODE_PRIVATE;

public class SetAndGetDataStorageIntern {

    CustomerPresenterImpl customerPresenter;
    Context context;
    SharedPreferences sharedPreferences;

    String TAG = "SetAndGetDataStorageIntern";
    public SetAndGetDataStorageIntern(Context context) {
        this.customerPresenter = customerPresenter;
        this.context = context;
    }
    public void saveDataUser(String key, String jsonStirng ){

        Log.v(TAG, "saveDataUser key: "+ key+" Data: " + jsonStirng);
        sharedPreferences = this.context.getSharedPreferences(key, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, jsonStirng);
        editor.commit();
    }

    public String getDataUser(String key){
        Log.v(TAG, "getDataUser key: "+ key);
        sharedPreferences = this.context.getSharedPreferences(key, MODE_PRIVATE);
        String jsonUser = "";
        String restoredText = sharedPreferences.getString(key, null);
        if (restoredText != null) {
            jsonUser = sharedPreferences.getString(key, "No name defined");
        }
        Log.v(TAG, "getDataUser key: "+ key+" Data: " + jsonUser);
        return jsonUser;
    }

}
