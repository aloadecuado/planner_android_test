package com.planner.planner.UTIL;

public interface GlobalConst {

    public static String KURL = "https://plannen.com/";
    public static String KURLSignIn = KURL + "appServices/test/login.php?email={email}&pass={pass}";
    public static String KURLListCutomer = KURL + "appServices/test/customers.php?userId={UserId}";
    public static String KEYSaveDataUser = "Planner123User";
    public static String KEYSaveDataListUsers = "Planner123ListUsers";
    public static String KEYSaveDataLogs = "Planner123Logs";


}
