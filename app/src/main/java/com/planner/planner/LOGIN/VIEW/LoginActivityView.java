package com.planner.planner.LOGIN.VIEW;


import com.planner.planner.LOGIN.MODEL.UserModel;

public interface LoginActivityView {
    void logInSucces(UserModel userModel);
    void showError(String error);
}
