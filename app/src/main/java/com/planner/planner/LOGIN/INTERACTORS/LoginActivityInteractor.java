package com.planner.planner.LOGIN.INTERACTORS;

import android.content.Context;
import android.widget.LinearLayout;

public interface LoginActivityInteractor {
    void logIn(Context context, LinearLayout linearLayout, String email, String password);
    void getAutoLogIn();
}
