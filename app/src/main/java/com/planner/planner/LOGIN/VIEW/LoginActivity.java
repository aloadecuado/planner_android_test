package com.planner.planner.LOGIN.VIEW;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.planner.planner.LOGIN.MODEL.UserModel;
import com.planner.planner.LOGIN.PRESENTERS.LoginActivityPresenterImpl;
import com.planner.planner.MENU.MenuActivity;
import com.planner.planner.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import static com.planner.planner.UTIL.GlobalVar.USEROBJECT;

public class LoginActivity extends AppCompatActivity implements LoginActivityView {

    @BindView(R.id.etEmail)
    EditText etEmail;

    @BindView(R.id.etPass)
    EditText etPass;

    @BindView(R.id.lyGeneral)
    LinearLayout lyGeneral;

    @BindView(R.id.pbSignIn)
    ProgressBar pbSignIn;
    UserModel UserModel;
    LoginActivityPresenterImpl loginActivityPresenterImpl;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
        loginActivityPresenterImpl = new LoginActivityPresenterImpl(this,this);
        loginActivityPresenterImpl.getAutoLogIn();
        pbSignIn.setVisibility(View.GONE);
        lyGeneral.setEnabled(true);

    }

    @OnClick(R.id.btSignIn)
    public void SignIn(){
        pbSignIn.setVisibility(View.VISIBLE);
        lyGeneral.setEnabled(false);
        loginActivityPresenterImpl.logIn(this, lyGeneral, etEmail.getText().toString(), etPass.getText().toString());
    }

    @Override
    public void logInSucces(UserModel userModel) {
        pbSignIn.setVisibility(View.GONE);
        lyGeneral.setEnabled(true);
        UserModel = userModel;

        USEROBJECT = UserModel;
        Intent intent = new Intent(this, MenuActivity.class);
        startActivity(intent);
        this.finish();
    }

    @Override
    public void showError(String error) {

        pbSignIn.setVisibility(View.GONE);
        lyGeneral.setEnabled(true);
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }
}
