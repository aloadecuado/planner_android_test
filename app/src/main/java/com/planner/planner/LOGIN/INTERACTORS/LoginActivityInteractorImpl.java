package com.planner.planner.LOGIN.INTERACTORS;

import android.content.Context;
import android.widget.LinearLayout;

import com.planner.planner.LOGIN.PRESENTERS.LoginActivityPresenter;
import com.planner.planner.LOGIN.REPOSITORY.LoginRepositoryImpl;

public class LoginActivityInteractorImpl implements LoginActivityInteractor {

    LoginActivityPresenter loginActivityPresenter;
    LoginRepositoryImpl loginRepository;
    public LoginActivityInteractorImpl(Context context, LoginActivityPresenter loginActivityPresenter) {
        this.loginActivityPresenter = loginActivityPresenter;
        this.loginRepository = new LoginRepositoryImpl(context, this.loginActivityPresenter);
    }

    @Override
    public void logIn(Context context, LinearLayout linearLayout, String email, String password) {

        this.loginRepository.LogIn(context, linearLayout, email, password);
    }

    @Override
    public void getAutoLogIn() {
        this.loginRepository.getAutoLogIn();
    }
}
