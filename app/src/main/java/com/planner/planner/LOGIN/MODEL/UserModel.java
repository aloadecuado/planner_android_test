package com.planner.planner.LOGIN.MODEL;

public class UserModel {

    private String userId = "";
    private String resultMessage = "";

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getResultMessage() {
        return resultMessage;
    }

    public void setResultMessage(String resultMessage) {
        this.resultMessage = resultMessage;
    }

    public boolean isSucces() {
        return succes;
    }

    public void setSucces(boolean succes) {
        this.succes = succes;
    }

    private boolean succes = false;

    public UserModel(String userId, String resultMessage, boolean succes) {
        this.userId = userId;
        this.resultMessage = resultMessage;
        this.succes = succes;
    }
}
