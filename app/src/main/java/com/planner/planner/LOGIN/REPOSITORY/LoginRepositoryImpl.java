package com.planner.planner.LOGIN.REPOSITORY;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.LinearLayout;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.planner.planner.LOGIN.MODEL.UserModel;
import com.planner.planner.LOGIN.PRESENTERS.LoginActivityPresenter;

import com.planner.planner.UTIL.*;
import static com.planner.planner.UTIL.GlobalConst.KEYSaveDataUser;
import static com.planner.planner.UTIL.GlobalConst.KURLSignIn;


public class LoginRepositoryImpl implements LogInRepository {

    LoginActivityPresenter loginActivityPresenter;
    Context context;

    SetAndGetDataStorageIntern setAndGetDataStorageIntern;

    String TAG = "LoginRepositoryImpl";
    public LoginRepositoryImpl(Context context, LoginActivityPresenter loginActivityPresenter) {
        this.loginActivityPresenter = loginActivityPresenter;
        this.context = context;
        setAndGetDataStorageIntern = new SetAndGetDataStorageIntern(this.context);
    }

    @Override
    public void LogIn(Context context, LinearLayout linearLayout, String email, String password) {

        Log.d(TAG, "email: " + email + " password: " + password);
        String jsonData = setAndGetDataStorageIntern.getDataUser(KEYSaveDataUser);
        if (!jsonData.equals("")) {

            UserModel userModel = new Gson().fromJson(jsonData, UserModel.class);
            Log.v(TAG, "data save user: " + jsonData);
            if (userModel.isSucces()) {
                loginActivityPresenter.logInSucces(userModel);
            } else {
                loginActivityPresenter.showError(userModel.getResultMessage());
            }
            return;
        }

            if (new ValidationClass().ValidacionCamposVacios(context, linearLayout)) {
                RequestQueue queue = Volley.newRequestQueue(context);

                String url = KURLSignIn.replace("{email}", email).replace("{pass}", password);
                Log.v(TAG, "url get list: " + url);
                StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.v(TAG, "data online user: " + response);
                                UserModel userModel = new Gson().fromJson(response, UserModel.class);
                                ;
                                if (userModel.isSucces()) {
                                    setAndGetDataStorageIntern.saveDataUser(KEYSaveDataUser, response);
                                    loginActivityPresenter.logInSucces(userModel);
                                } else {
                                    loginActivityPresenter.showError(userModel.getResultMessage());
                                }

                            }

                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.v(TAG, "data online user error: " + error.getLocalizedMessage());
                                loginActivityPresenter.showError(error.getLocalizedMessage());
                            }
                        });

                queue.add(stringRequest);
            } else {
                loginActivityPresenter.showError("");
            }

    }

    @Override
    public void getAutoLogIn() {
        String jsonData = setAndGetDataStorageIntern.getDataUser(KEYSaveDataUser);
        if (!jsonData.equals("")) {

            UserModel userModel = new Gson().fromJson(jsonData, UserModel.class);
            if (userModel.isSucces()) {
                loginActivityPresenter.logInSucces(userModel);
            } else {
                loginActivityPresenter.showError(userModel.getResultMessage());
            }
            return;
        }
    }


}
