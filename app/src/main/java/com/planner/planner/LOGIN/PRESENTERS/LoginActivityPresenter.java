package com.planner.planner.LOGIN.PRESENTERS;

import android.content.Context;
import android.widget.LinearLayout;

import com.planner.planner.LOGIN.MODEL.UserModel;


public interface LoginActivityPresenter {
    void logIn(Context context, LinearLayout linearLayout, String email, String password);
    void getAutoLogIn();

    void logInSucces(UserModel userModel);
    void showError(String error);
}
