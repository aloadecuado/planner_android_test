package com.planner.planner.LOGIN.REPOSITORY;

import android.content.Context;
import android.widget.LinearLayout;

public interface LogInRepository {

    void LogIn(Context context, LinearLayout linearLayout, String email, String password);
    void getAutoLogIn();
}
