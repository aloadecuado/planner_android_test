package com.planner.planner.LOGIN.PRESENTERS;

import android.content.Context;
import android.widget.LinearLayout;

import com.planner.planner.LOGIN.INTERACTORS.LoginActivityInteractorImpl;
import com.planner.planner.LOGIN.MODEL.UserModel;
import com.planner.planner.LOGIN.VIEW.LoginActivityView;


public class LoginActivityPresenterImpl implements LoginActivityPresenter {
    LoginActivityView loginActivityView;
    LoginActivityInteractorImpl loginActivityInteractor;
    public LoginActivityPresenterImpl(Context context, LoginActivityView loginActivityView) {
        this.loginActivityView = loginActivityView;
        loginActivityInteractor = new LoginActivityInteractorImpl(context,this);
    }

    @Override
    public void logIn(Context context, LinearLayout linearLayout, String email, String password) {
        loginActivityInteractor.logIn(context, linearLayout, email, password);
    }

    @Override
    public void getAutoLogIn() {
        loginActivityInteractor.getAutoLogIn();
    }

    @Override
    public void logInSucces(UserModel userModel) {
        this.loginActivityView.logInSucces(userModel);
    }

    @Override
    public void showError(String error) {
        this.loginActivityView.showError(error);
    }
}
